#ifndef SOLUTION_H
#define SOLUTION_H
#include "route.hpp"
#include "instance.hpp"

struct StatusSolution{
  ld rootObj = -1, rootTime = -1, finalTime = 0;
  int L1 = 1, L2 = 1, Z = 0;
  string status;
};

class Solution{
  Instance* instance;
  set<Route*, Route::lessRouteEQ> routes;
  ll cost;

  public:
  Solution(Instance* instance);
  Solution(const Solution& sol);
  ~Solution();

  void addRoute(Route* route);
  void remRoute(Route* route);

  void check();
  void printSolution(StatusSolution statusSol);

  static Solution* buildSolution(Instance* instance);
  void fix();
  void findRoute(set<int>& rem, Route* route);

  vector<Route*> getRoutes() const;
  ll getCost();
  int getSize();

  bool operator < (const Solution& sol2) const{
    return cost < sol2.cost;
  }
};

#endif
