#ifndef SOLVER_H
#define SOLVER_H

#include "/opt/gurobi952/linux64/include/gurobi_c++.h"
#include "pricing.hpp"
#include "instance.hpp"
#include "route.hpp"
#include "solution.hpp"

struct Node {
  int cl1, cl2;
  vi rem, nxt1, nxt2;
  bool state;

  Node(int cl1, int cl2): cl1(cl1), cl2(cl2), state(false){}
};

struct NodeRF{
  vi addCls, addRoutes;
  vector<Route*> routes;
  int sz;
  NodeRF(): sz(0){}
};

class Solver{
  int N, R, M, lvl = 0, timeCurrent = 0, LPTime = 0, pricingTime = 0, bestInteger, lastPrint = 0;
  int integers, nonZero, iterationsBP = 0, columnGenerations = 0;
  Instance* instance;
  GRBModel* model;
  CPM* cpm;
  Pricing* pricing;
  set<Route*, Route::lessRouteEQ> actRoutes;
  vector<set<int>> clToIds;
  vector<Route*>& idToRoute;
  const vvi& cost;
  GRBVar* vars = NULL;
  GRBConstr* constrs = NULL;
  vi has, ids;
  set<int> actCls;
  vvd dualP, afim;
  vvi nxt;
  vd primal, dual;
  ld objPrimal, objDual, lastVal = 0, bestLB = 1;
  vector<Node> st;
  Solution* bestSol = NULL, *partial = NULL;
  chrono::steady_clock::time_point begin;
  StatusSolution statusSol;
  bool bounded = false, masterCol = false, optimal = false, inRoot = true, useCut = true;
  ofstream valueStream;

  void initModel();
  void remConflicts(int cl1, int cl2, vi& toRem);
  bool addRoute(Route* route);
  void mergeCls(int cl1, int cl2);
  void remRoute(Route* route, bool update = true);
  bool addCuts();

  template<class T> void Unique(vector<T>& v);
  ld getObj();
  ld updateTime();
  ld getTimeFrat();
  void clearModel();
  void updateLB();
  void updateUB();

  void fixPatsVar(vector<NodeRF>& st);
  void remBackNodeRF(vector<NodeRF>& st);
  bool relaxAndFix();
  bool colGen();
  ld Optimize(bool cutting = false);
  ld runPL();
  bool PH();

  int heaviest(int id);
  bool isElementar(int id);

  void updateIntegers();
  void getPrimal();
  void getDual();
  void recoverSol();

  void remRouteList(const vi& routeList, bool update);
  void addCls(vi cls, const vi& routes, bool force);
  vi remCls(vi cls);

  void print(bool force);

  bool canBeAdd(Route* pat);

  bool solveRoot();

  void checkModelValid();
  bool fixOneVar();

  void remBackNode();
  void swapState();
  int backtracking();
  bool fixST(ld& curObj);
  bool relax(ld& curObj);
  void BranchAndPrice();

  public:
  Solver(Instance* instance);
  ~Solver();
  void Solve();

  ll getCost(Route* route);
};

#endif
