#ifndef ROUTE_H
#define ROUTE_H

#include "util.hpp"
#include "instance.hpp"

struct Route{
  struct equalRoute{
    bool operator() (Route* const& r1, Route* const& r2) const{
      return r1->cap == r2->cap && r1->cls == r2->cls;
    }
  };

  struct lessRouteEQ{
    bool operator() (Route* const& r1, Route* const& r2) const{
      if(r1->cap != r2->cap)
        return r1->cap > r2->cap;
      return r1->cls < r2->cls;
    }
  };

  inline static vi d;
  inline static ll idCnt = 0;
  inline static set<Route*, lessRouteEQ> allRoutes;
  inline static vector<Route*> idToRoute;
  inline static vector<bool> dels;

  vi cls;
  int cap;
  ld val;
  ll id;
  ll cost;

  Route(int cl = -1): cap(0), val(0), id(idCnt++), cost(-1){
    if(cl != -1)
      Add(cl);
    dels.push_back(false);
    assert(SZ(dels) == id + 1);
    idToRoute.push_back(this);
  }

  Route(const Route& r2): cls(r2.cls), cap(r2.cap), val(r2.val), id(idCnt++), cost(r2.cost){
    dels.push_back(false);
    assert(SZ(dels) == id + 1);
    idToRoute.push_back(this);
  }

  ~Route(){
    assert(!dels[this->id]);
    dels[this->id] = true;
  }

  void Add(int cl){
    cls.push_back(cl);
    cap += d[cl];
  }

  void Pop(){
    cap -= d[cls.back()];
    cls.pop_back();
  }

  int size(){
    return SZ(cls);
  }

  void updateCost(const Instance* instance){
    cost = instance->cost[0][cls[0]] + instance->cost[cls.back()][instance->R];
    FOR(i, SZ(cls) - 1)
      cost += instance->cost[cls[i]][cls[i + 1]];
  }

  void check(const Instance* instance){
    assert(cap <= capacity);

    int R = instance->R;
    int cur = 0, q = 0, time = 0;
    cls.push_back(R);

    for(int nxt : cls){
      time += instance->dist[cur][nxt];
      time = max<int>(time, instance->e[nxt]);
      assert(time <= instance->l[nxt]);
      q += d[nxt];
      assert(q <= capacity);
      cur = nxt;
    }
    assert(q == cap);

    cls.pop_back();
  }
};

#endif