#include "CutPlaneManager.hpp"

CPM::CPM(GRBModel*& model, const int& R, bool& bounded, const vi& has, const vi& ids,
  const vd& primal, const vvd& dual, const vector<set<int>>& clToIds):
  model(model), R(R), bounded(bounded), has(has), ids(ids), primal(primal),
  dual(dual), clToIds(clToIds), clsToSRAll(R, vector<IM>()), clsToSR(R, vector<IM>()){}

int CPM::getActCuts(){
  return actCuts;
}

int CPM::getCuts(){
  return SZ(mapSR);
}

int CPM::getMaxActCuts(){
  return mxActCuts;
}

void CPM::updMaxActCuts(){
  mxActCuts = max(mxActCuts, actCuts);
}

void CPM::addSRCoef(GRBColumn& col, Route* route){
  vi cnt(SZ(setsSRAll), 0);

  for(int cl : route->cls)
    for(IM im : clsToSRAll[cl])
      cnt[im.i] += im.m;

  FOR(id, SZ(cnt))
    if(cnt[id] >= setsSRAll[id].d && mapSR[id] >= -1){
      auto constr = model->getConstrByName("c" + to_string(id));
      col.addTerm(cnt[id] / setsSRAll[id].d, constr);
    }
}

ld CPM::updateVal(Route* route){
  fill(all(seenSR), 0);
  route->val = dual[0][route->cls[0]] + dual[route->cls.back()][R];
  FOR(i, SZ(route->cls) - 1)
    route->val += dual[route->cls[i]][route->cls[i + 1]];

  for(int cl : route->cls){
    for(IM im: clsToSR[cl]){
      int id = im.i;
      seenSR[id] += im.m;
      if(seenSR[id] >= setsSR[id].d){
        route->val += dualSR[id];
        seenSR[id] -= setsSR[id].d;
      }
    }
  }
  return route->val;
}

vector<IM> CPM::getIdsCut(Cut cut){
  map<int, int> acumIdx;
  for(IM im : cut)
    for(int id : clToIds[im.i])
      acumIdx[id] += im.m * count(all(Route::idToRoute[id]->cls), im.i);

  vector<IM> ret;
  for(auto pr : acumIdx)
    if(pr.second >= cut.d)
      ret.push_back({pr.first, pr.second / cut.d});
  return ret;
}

pair<ld, vector<IM>> CPM::calcCut(Cut cut, map<int, int>& invIds){
  ld acum = 0;
  vector<IM> idMultCuts = getIdsCut(cut);
  for(IM im : idMultCuts)
    acum += im.m * primal[invIds[im.i]];
  return {acum, idMultCuts};
}

multimap<ld, pair<Cut, vector<IM>>, greater<ld>> CPM::calcCuts(map<int, int>& invIds){
  int MX = INT_MAX / 2;//N * capacity;

  vvd afim(R, vd(R, 0));
  FOR(h, SZ(primal)){
    vi& cls = Route::idToRoute[ids[h]]->cls;
    FOR(i, SZ(cls)){
      REV(j, i){
        if(cls[i] != cls[j]){
          afim[cls[i]][cls[j]] += primal[h];
          afim[cls[j]][cls[i]] += primal[h];
        }
      }
    }
  }

  vector<vi> rel(R);
  FORI(cl1, R - 1)
    if(has[cl1])
      for(int cl2 = cl1 + 1; cl2 < R; cl2++)
        if(has[cl2] && afim[cl1][cl2] > EPS8)
          rel[cl1].push_back(cl2);

  int cnt = 0;
  multimap<ld, pair<Cut, vector<IM>>, greater<ld>> cuts;
  FORI(cl1, R - 1){
    if(cnt > MX)
      break;

    for(int cl2 : rel[cl1]){
      if(cnt > MX)
        break;

      vi ls;
      cnt += SZ(rel[cl1]) + SZ(rel[cl2]);
      std::set_intersection(all(rel[cl1]), all(rel[cl2]), back_inserter(ls));
      for(int cl3 : ls){
        if(cnt > MX)
          break;
        ld val = afim[cl1][cl2] + afim[cl2][cl3] + afim[cl3][cl1];

        if(val > 1 + EPS8){
          cnt += SZ(clToIds[cl1]) + SZ(clToIds[cl2]) + SZ(clToIds[cl3]);
          vector<IM> ims = {{cl1, 1}, {cl2, 1}, {cl3, 1}};
          sort(all(ims));

          Cut cut(ims, 1, 2);
          pair<ld, vector<IM>> valIneq = calcCut(cut, invIds);

          if(valIneq.first > cut.RHS + EPS8)
            cuts.insert({valIneq.first - cut.RHS, {cut, valIneq.second}});
        }
      }
    }
  }
  return cuts;
}

int CPM::addCutPool(Cut& cut){
  if(cutsPool.count(cut)){
    cut = *cutsPool.find(cut);
    countSR[cut.id] = 0;
    mapSR[cut.id] = -1;
  }else{
    int id = cut.id = SZ(mapSR);
    mapSR.push_back(-1);
    countSR.push_back(0);
    setsSRAll.push_back(cut);
    for(IM im : setsSRAll.back())
      clsToSRAll[im.i].push_back({id, im.m});
    cutsPool.insert(cut);
  }
  return cut.id;
}

bool CPM::genCuts(){
  lastAdded.clear();

  map<int, int> invIds;
  FOR(i, SZ(ids))
    invIds[ids[i]] = i;

  auto cuts = calcCuts(invIds);

  /*if(cuts.empty())
    cuts = calcCutsUp(invIds);*/

  if(cuts.size() > 5)
    cuts.erase(next(cuts.begin(), 5), cuts.end());

  if(cuts.empty())
    return false;

  GRBVar* vars = model->getVars();

  for(auto& pr : cuts){
    Cut& cut = pr.second.first;
    int id = addCutPool(cut);
    lastAdded.push_back(cut);

    GRBLinExpr constr;
    for(IM im : pr.second.second)
      constr += im.m * vars[invIds[im.i]];
    model->addConstr(constr, GRB_LESS_EQUAL, cut.RHS, "c" + to_string(id));
  }
  delete[] vars;

  return true;
}

int CPM::setDualSR(ld val, int cl){
  if(-val > EPS13){
    int id = SZ(dualSR);
    mapSR[cl] = id;
    countSR[cl] = 0;
    dualSR.push_back(-val);
    seenSR.push_back(0);
    setsSR.push_back(setsSRAll[cl]);
    for(IM im : setsSR.back())
      clsToSR[im.i].push_back({id, im.m});
    countSR[cl] += -val < EPS9;
  }else{
    countSR[cl]++;
    mapSR[cl] = -1;
  }
  actCuts++;
  return setsSRAll[cl].RHS;
}

void CPM::resetSR(){
  actCuts = 0;
  dualSR.clear();
  setsSR.clear();
  seenSR.clear();
  FORI(cl, R - 1)
    clsToSR[cl].clear();
}

void CPM::checkCuts(){
  FOR(id, SZ(setsSRAll)){
    if(mapSR[id] >= -1){
      bool good = true;
      for(IM im : setsSRAll[id])
        good &= has[im.i];
      if(!good || countSR[id] > 100){
        bounded = false;
        mapSR[id] = -2;
        auto constr = model->getConstrByName("c" + to_string(id));
        model->remove(constr);
      }
    }
  }
}

void CPM::clearCuts(){
  FOR(id, SZ(setsSRAll)){
    if(mapSR[id] >= -1){
      bounded = false;
      mapSR[id] = -2;
      auto constr = model->getConstrByName("c" + to_string(id));
      model->remove(constr);
    }
  }
}

void CPM::removeLastAdded(){
  bounded = false;
  for(auto cut : lastAdded){
    int id = cut.id;
    mapSR[id] = -2;
    auto constr = model->getConstrByName("c" + to_string(id));
    model->remove(constr);
  }
  lastAdded.clear();
}