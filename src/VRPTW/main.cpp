#include "solver.hpp"
#include "fileHandler.h"

int main(int argc, char* argv[]){
  if(argc != 2){
    cerr << "Numero de argumentos invalido" << endl;
    exit(1);
  }

  string outdir = outdirName(argv[0]);
  string path = argv[1], prefix = outdir + "/" + getFileName(path);
  Instance* instance = Instance::readInstance(path, prefix);

  createDir(outdir);

  Solver* solver = new Solver(instance);
  solver->Solve();

  delete solver;
  delete instance;

  FOR(i, SZ(Route::dels))
    assert(Route::dels[i]);
}
